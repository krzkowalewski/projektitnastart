<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html" ; charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Projekt It na start!</title>

</head>


<body>
<div class="container">
    <div class="row">
        <div class="col">
            <h2>Lista projekt�w</h2>
            </br>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Opis projektu</th>
                    <th scope="col">Tw�rca</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${projects}" var="project">
                    <tr>
                        <td>${project.name}</td>
                        <td>${project.description}</td>
                        <td>${project.author}</td>
                        <td><a href="<c:url value='/edit-${project.projectId}-project' />">
                            <button type="button" class="btn btn-warning">Modyfikuj</button></a>
                            <a href="<c:url value='/delete-${project.projectId}-project' />">
                                <button type="button" class="btn btn-danger">Delete</button>
                        </a></td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
        <br/>
    <a href="<c:url value='/new' />">
        <button type="button" class="btn btn-primary btn-lg">Dodaj nowy projekt</button>
    </a>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>