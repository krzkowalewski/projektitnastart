<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Dodawanie projektu</title>

    <style>

        .error {
            color: #ff0000;
        }
    </style>

</head>

<body>

<h2>Add Form</h2>

<form:form method="POST" modelAttribute="project">
    <form:input type="hidden" path="projectId" id="projectId"/>
    <table>
        <tr>
            <td><label for="name">Nazwa projektu: </label> </td>
            <td><form:input path="name" id="name"/></td>
            <td><form:errors path="name" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="description">Opis projektu: </label> </td>
            <td><form:input path="description" id="description"/></td>
            <td><form:errors path="description" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="author">Autor: </label> </td>
            <td><form:input path="author" id="author"/></td>
            <td><form:errors path="author" cssClass="error"/></td>
        </tr>

        <tr>
        <td><label for="addDate">data dodania: </label> </td>
        <td><form:input path="addDate" id="addDate"/></td>
        <td><form:errors path="addDate" cssClass="error"/></td>
    </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="zapisz"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="dodaj"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
<br/>
<br/>
Go back to <a href="<c:url value='/all' />">Wszystkie projekty</a>
</body>
</html>