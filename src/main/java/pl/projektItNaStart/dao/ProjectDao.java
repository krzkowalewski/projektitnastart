package pl.projektItNaStart.dao;


import pl.projektItNaStart.model.Project;

import java.util.List;

public interface ProjectDao {

    Project findByProjectId(int projectId);

    void addProject(Project project);

    void deleteProjectByProjectId(int projectId);

    List<Project> findAllProjects();


}
