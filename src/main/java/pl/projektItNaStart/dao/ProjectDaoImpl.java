package pl.projektItNaStart.dao;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import pl.projektItNaStart.model.Project;


import java.util.List;

@Repository("projectDao")
public class ProjectDaoImpl extends AbstractDao<Integer, Project> implements ProjectDao {

    public void addProject(Project project) {persist(project);}

    public Project findByProjectId(int projectId) {
        return getByKey(projectId);
    }


    public void deleteProjectByProjectId(int projectId) {
        int query = getSession().createSQLQuery("delete from Projects where projectId = :id").setParameter("id", projectId).executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<Project> findAllProjects() {
        Criteria criteria = createEntityCriteria();
        return (List<Project>) criteria.list();
    }
}
