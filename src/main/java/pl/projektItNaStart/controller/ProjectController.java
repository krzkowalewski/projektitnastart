package pl.projektItNaStart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.projektItNaStart.model.Project;
import pl.projektItNaStart.service.ProjectService;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;


@Controller
@RequestMapping("/")
public class ProjectController {


    @Autowired
    ProjectService service;

    @Autowired
    MessageSource messageSource;


    @RequestMapping(value = {"/all"}, method = RequestMethod.GET)
    public String findAllProjects(ModelMap model) {

        List<Project> projects = service.findAllProjects();
        model.addAttribute("projects", projects);
        return "allprojects";
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    public String addProject(ModelMap model) {
        Project project = new Project();
        model.addAttribute("project", project);
        model.addAttribute("edit", false);
        return "addform";
    }


    @RequestMapping(value = {"/new"}, method = RequestMethod.POST)
    public String addProject(@Valid Project project, BindingResult result,
                             ModelMap model) {

        if (result.hasErrors()) {
            return "addform";
        }

        service.addProject(project);

        model.addAttribute("success", "Project " + project.getName() + " registered successfully");
        return "redirect:/all";
    }

    @RequestMapping(value = {"/edit-{projectId}-project"}, method = RequestMethod.GET)
    public String editProject(@PathVariable int projectId, ModelMap model) {
        Project project = service.findByProjectId(projectId);
        model.addAttribute("project", project);
        model.addAttribute("edit", true);
        return "addform";
    }

    @RequestMapping(value = {"/edit-{projectId}-project"}, method = RequestMethod.POST)
    public String updateEmployee(@Valid Project project, BindingResult result,
                                 ModelMap model, @PathVariable int projectId) {

        if (result.hasErrors()) {
            return "addform";
        }

        service.updateProject(project);

        model.addAttribute("success", "Project " + project.getName() + " updated successfully");
        return "redirect:/all";
    }

    @RequestMapping(value = {"/delete-{projectId}-project"}, method = RequestMethod.GET)
    public String deleteEmployee(@PathVariable int projectId) {
        service.deleteProjectByProjectId(projectId);
        return "redirect:/all";
    }

}
