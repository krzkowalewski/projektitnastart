package pl.projektItNaStart.service;


import pl.projektItNaStart.model.Project;

import java.util.List;

public interface ProjectService {

    Project findByProjectId(int projectId);

    void addProject(Project project);

    void deleteProjectByProjectId(int projectId);

    void updateProject(Project project);

    List<Project> findAllProjects();


}
