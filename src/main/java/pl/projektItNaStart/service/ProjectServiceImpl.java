package pl.projektItNaStart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.projektItNaStart.dao.ProjectDao;
import pl.projektItNaStart.model.Project;

import java.util.List;

@Service("projectService")
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectDao dao;


    public Project findByProjectId(int projectId) {
        return dao.findByProjectId(projectId);
    }


    public void addProject(Project project) {
        dao.addProject(project);
    }


    public void deleteProjectByProjectId(int projectId) {
        dao.deleteProjectByProjectId(projectId);
    }

    @Override
    public void updateProject(Project project) {
        Project entity = dao.findByProjectId(project.getProjectId());
        if (entity != null) {
            entity.setName(project.getName());
            entity.setDescription(project.getDescription());
            entity.setAuthor(project.getAuthor());
        }
    }


    public List<Project> findAllProjects() {
        return dao.findAllProjects();
    }
}
