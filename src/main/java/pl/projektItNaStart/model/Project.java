package pl.projektItNaStart.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "Projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int projectId;

    @Size(min = 3, max = 30)
    @Column(name = "NAME", nullable = false)
    private String name;

    @Temporal(TemporalType.DATE)
    @Column(name = "ADD_DATE")
    private Date addDate;

    @Size(min = 5, max = 500)
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "AUTHOR", nullable = false)
    private String author;

    public Project() {

    }

    public Project(String name, String description, String author, Date addDate) {
        this.name = name;
        this.description = description;
        this.author = author;
        this.addDate = addDate;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Projects{" +
                "Project id=" + projectId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", author=" + author +
                "}";
    }
}
